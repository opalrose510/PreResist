<!DOCTYPE html>
<html>


<head>
<title>PreResist.org</title>

</head>
<body class="w3-content" style="max-width:1300px">
<?php



   include('../../db_settings.php');
   $link = mysqli_connect("50.62.209.38:3306", $db_user, $db_pass)
   or die("Could not connect: " . mysqli_error());
   unset($db_user, $db_pass);
   include('../../db_queries.php');
   if($_POST['action'] == 'Defer') {
	   header("Location: index.php");
	   exit();
   } elseif($_POST['action'] == 'Disapprove') {
		disapprove_by_id($link, $_POST['id']);
   } else {
		approve_by_id($link, $_POST['id']);
   }
	header("Location: index.php");
?>
</body>
</html>
